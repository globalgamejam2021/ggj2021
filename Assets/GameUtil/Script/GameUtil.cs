﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;

public class GameUtil
{
    public int Lv_Open = 1;            // 7 = end game
    private static GameUtil gameUtil;

    public static GameUtil GetInstance()
    {
        if (gameUtil == null)
            gameUtil = new GameUtil();
        return gameUtil;
    }

    private GameUtil()
    {
    }

    // Timestamp 轉換成 DateTime
	public static DateTime UnixTimeStampToDateTime(long unixTimeStamp)
    {
        DateTime dtDateTime = new DateTime(1970, 1, 1, 8, 0, 0, 0, System.DateTimeKind.Utc);
        dtDateTime = dtDateTime.AddSeconds(unixTimeStamp);
        return dtDateTime;
    }
    
    // DateTime 轉換成 Timestamp
	public static long UnixDateTimeToTimeStamp(DateTime dateTime)
    {
        long unixTimestamp = (long)(dateTime.Subtract(new System.DateTime(1970, 1, 1, 8, 0, 0))).TotalSeconds;
        return unixTimestamp;
    }
}
