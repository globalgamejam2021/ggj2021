﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class AudioUtil : MonoBehaviour
{
    public static string GameStart = "8rice/osh";
    public static string GameSelect = "8rice/fell";
    public static string Char_Spell1 = "8rice/spell1";
    public static string Char_Spell2 = "8rice/spell2";

    public static string Char_Die1 = "8rice/angry";
    public static string Char_Die2 = "8rice/crazy";
    public static string Char_Die3 = "8rice/cry";
    public static string Char_Jump = "8rice/jump";
    public static string Char_Wa = "8rice/wa";
    public static string ItemGet = "8rice/cute";
    public static string GameEnd = "8rice/a_h";
    public AudioSource audioSource;
    private static AudioUtil audioUtil;

    public static AudioUtil GetInstance()
    {
        if (audioUtil == null)
            audioUtil = new AudioUtil();
        return audioUtil;
    }

    private AudioUtil()
    {
    }

    public void PlayOneShot(string _str) {
        if(audioSource == null)
        {
            GameObject _go = new GameObject("SFX");
            audioSource = _go.AddComponent<AudioSource>();
        }
        audioSource.PlayOneShot(Resources.Load("Audio/" + _str) as AudioClip);
    }

    public void PlaySpell() {
        if(audioSource == null)
        {
            GameObject _go = new GameObject("SFX");
            audioSource = _go.AddComponent<AudioSource>();
        }
        switch(Random.Range(0, 2))
        {
            default:
                audioSource.PlayOneShot(Resources.Load("Audio/" + AudioUtil.Char_Spell1) as AudioClip);
            break;
            case 1:
                audioSource.PlayOneShot(Resources.Load("Audio/" + AudioUtil.Char_Spell2) as AudioClip);
            break;
        }
        
    }

    public void PlayDie() {
        if(audioSource == null)
        {
            GameObject _go = new GameObject("SFX");
            audioSource = _go.AddComponent<AudioSource>();
        }
        switch(Random.Range(0, 3))
        {
            default:
                audioSource.PlayOneShot(Resources.Load("Audio/" + AudioUtil.Char_Die1) as AudioClip);
            break;
            case 1:
                audioSource.PlayOneShot(Resources.Load("Audio/" + AudioUtil.Char_Die2) as AudioClip);
            break;
            case 2:
                audioSource.PlayOneShot(Resources.Load("Audio/" + AudioUtil.Char_Die3) as AudioClip);
            break;
        }
        
    }
}
