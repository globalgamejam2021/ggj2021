﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum TimerType{
	MinToMicro = 0,
	SecToNano = 1,
	Normal = 2
}
public class TimerGlobalController : MonoBehaviour {

	private float time = 0;
	public float Time {
		get {return time;}
	}

	public bool isCountDownTime = false;

	public float CountDownTimeMax = 0;

	private bool isStartTimer = false;
	public bool isStartingTime {
		get { return isStartTimer; }
	}

	public TimerType timerType = TimerType.MinToMicro;

	public Text txtTimer;

	public bool isDebugMode = false;


	// Use this for initialization
	void Start () {
		UpdateTimer();
	}
	
	// Update is called once per frame
	void Update () {
		if(isStartTimer) {
			time += UnityEngine.Time.deltaTime;
			UpdateTimer();
		}
	}

	void OnGUI()
	{
		if(!isDebugMode)
			return;
		GUIStyle myStyle = GUI.skin.button;
    	myStyle.fontSize = 30;
		if (GUI.Button(new Rect(10, 10, 250, 50), "Start Timer", myStyle))
            StartTimer();
		if (GUI.Button(new Rect(10, 70, 250, 50), "Stop Timer", myStyle))
            StopTimer();
		if (GUI.Button(new Rect(10, 130, 250, 50), "Reset Timer", myStyle))
            ResetTimer();
	}

	public void Init(Text _text) {
		txtTimer = _text;
		CountDownTimeMax = 0;
		isCountDownTime = false;
	}

	public void Init(Text _text, float _CountDownTimeMax) {
		txtTimer = _text;
		isCountDownTime = true;
		CountDownTimeMax = _CountDownTimeMax;
	}

	public void StartTimer() {
		isStartTimer = true;
	}

	public void StopTimer() {
		isStartTimer = false;
	}


	public void ResetTimer()
	{
		time = 0;
		UpdateTimer();
	}

	private void UpdateTimer() {
		if(txtTimer != null)
		{
			if(!isCountDownTime)
				txtTimer.text = GetTime(timerType);
			else
				txtTimer.text = GetCountDownTime(timerType);
		}
	}

	public string GetTime(TimerType _type = TimerType.MinToMicro) {
		timerType = _type;
		return TimeToString(Time, _type);
	}

	public string GetCountDownTime(TimerType _type = TimerType.MinToMicro) {
		timerType = _type;
		if(CountDownTimeMax <= 0)
			return "no Max Time";
		float curLeftTime = CountDownTimeMax - Time;
		if(curLeftTime < 0)
		{
			curLeftTime = 0;
			StopTimer();
			TimeEnd();
		}
		return CountDownTimeToString(curLeftTime, _type);
	}

	private string TimeToString(float t, TimerType _type = TimerType.MinToMicro)
	{
		timerType = _type;
		int min = Mathf.FloorToInt (t / 60f);
		int Sec = Mathf.FloorToInt (t % 60f);
		int MicroSec = Mathf.FloorToInt ((t % 1f) * 100f);
		int NanoSec = Mathf.FloorToInt ((t % 0.01f) * 10000f);

		switch(_type)
		{
			default:
				return min.ToString("D2") + "'" + Sec.ToString ("D2") + "''" + MicroSec.ToString("D2");
			case TimerType.SecToNano:
				return Sec.ToString ("D2") + ":" + MicroSec.ToString("D2") + ":" + NanoSec.ToString("D2");
			case TimerType.Normal:
			{
				Sec = Mathf.FloorToInt (t);
				return Sec.ToString();
			}
		}
	}

	private string CountDownTimeToString(float t, TimerType _type = TimerType.MinToMicro)
	{
		timerType = _type;
		int min = Mathf.FloorToInt (t / 60f);
		int Sec = Mathf.FloorToInt (t % 60f);
		int MicroSec = Mathf.FloorToInt ((t % 1f) * 100f);
		int NanoSec = Mathf.FloorToInt ((t % 0.01f) * 10000f);

		switch(_type)
		{
			default:
				return min.ToString("D2") + "'" + Sec.ToString ("D2") + "''" + MicroSec.ToString("D2");
			case TimerType.SecToNano:
				return Sec.ToString ("D2") + ":" + MicroSec.ToString("D2") + ":" + NanoSec.ToString("D2");
			case TimerType.Normal:
			{
				Sec = Mathf.CeilToInt (t);
				return Sec.ToString();
			}
		}
		//int Sec = Mathf.CeilToInt (t);
		//return Sec.ToString();
	}

	private void TimeEnd() {
		// Add some function at here when game end
		Debug.Log("Add some function for game end");
	}
}
