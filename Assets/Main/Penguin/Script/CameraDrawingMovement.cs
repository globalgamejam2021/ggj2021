using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Penguin
{
    public class CameraDrawingMovement : MonoBehaviour
    {
        public GameController gameController;
        public float normalSpeed = 5;
        public float highSpeed = 15;
        private float curSpeed = 0;

        private Vector2 limitMin;
        private Vector2 limitMax;

        private twodotzero.tracking tracking;
        // Start is called before the first frame update
        void Start()
        {
            tracking = GetComponent<twodotzero.tracking>();
            tracking.ResetToTarget();
        }

        // Update is called once per frame
        void Update()
        {
            limitMin = new Vector2(tracking.leftLimit, tracking.bottomLimit);
            limitMax = new Vector2(tracking.rightLimit, tracking.topLimit);
            CameraMove();
        }

        void CameraMove() {
            if(gameController.curMode == Mode.Drawing)
            {
                if(Input.GetButton("Jump"))
                    curSpeed = highSpeed;
                else
                    curSpeed = normalSpeed;
                if (Input.GetAxisRaw("Horizontal") < 0 && transform.position.x > limitMin.x)
                {
                    transform.Translate(Vector3.left * Time.deltaTime * curSpeed);
                }
                if (Input.GetAxisRaw("Horizontal") > 0 && transform.position.x < limitMax.x)
                {
                    transform.Translate(Vector3.left * Time.deltaTime * -1 * curSpeed);
                }
                if (Input.GetAxisRaw("Vertical") < 0 && transform.position.y > limitMin.y)
                {
                    transform.Translate(Vector3.up * Time.deltaTime * -1 * curSpeed);
                }
                if (Input.GetAxisRaw("Vertical") > 0 && transform.position.y < limitMax.y)
                {
                    transform.Translate(Vector3.up * Time.deltaTime * curSpeed);
                }
            }
        }
    }
}
