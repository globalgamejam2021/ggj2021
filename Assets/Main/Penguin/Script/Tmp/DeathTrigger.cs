using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Penguin {
    public class DeathTrigger : MonoBehaviour
    {
        public GameController gameController;
        // Start is called before the first frame update
        void Start()
        {
            
        }

        // Update is called once per frame
        void Update()
        {
            
        }

        void OnTriggerStay2D(Collider2D other)
        {
            if(other.tag == "Player")
            {
                gameController.Replay();
            }
            /*
            if(other.gameObject.layer == 8)
            {
                other.GetComponent<BrushController>().RestoreInk();
                other.GetComponent<BrushController>().circleTimer.DestroyNow();
            }
            */
        }
    }
}
