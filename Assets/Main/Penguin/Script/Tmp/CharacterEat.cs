using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace Penguin {
    public class CharacterEat : MonoBehaviour
    {
        public GameController gameController;
        // Start is called before the first frame update
        void Start()
        {
            
        }

        // Update is called once per frame
        void Update()
        {
            
        }

        /// <summary>
        /// OnTriggerStay is called once per frame for every Collider other
        /// that is touching the trigger.
        /// </summary>
        /// <param name="other">The other Collider involved in this collision.</param>
        void OnTriggerStay2D(Collider2D other)
        {
            if(other.name == "Donut")
            {
                other.gameObject.SetActive(false);
                gameController.Win();
            }
        }        
    }
}
