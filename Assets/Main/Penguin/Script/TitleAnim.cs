using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.SceneManagement;
using UnityEngine.Video;

namespace Penguin {
    
    public class TitleAnim : MonoBehaviour
    {
        public Image imgTitle_cn, imgTitle_en;

        public VideoPlayer videoPlayer;
        public RawImage imgRaw;

        public Image imgBlack;
        public AudioSource audioSource;

        public bool isCanClick = false;

        private AudioUtil audioUtil;

        /// <summary>
        /// Awake is called when the script instance is being loaded.
        /// </summary>
        void Awake()
        {
            audioUtil = AudioUtil.GetInstance();
        }
        // Start is called before the first frame update
        void Start()
        {
            imgTitle_cn.transform.DOScale(Vector3.one, 0.5f).SetEase(Ease.InQuad);
            imgTitle_cn.DOFade(1, 0.3f);
            imgTitle_en.transform.DOScale(Vector3.one, 0.5f).SetEase(Ease.InQuad).SetDelay(0.75f);
            imgTitle_en.DOFade(1, 0.3f).SetDelay(0.75f).OnComplete(()=>{
                imgTitle_en.DOFade(1, 0f).SetDelay(0f).OnComplete(()=>{
                    imgTitle_en.DOFade(0.3f, 1f).SetLoops(-1, LoopType.Yoyo).SetId("title");
                    isCanClick = true;
                });
            });
            
            videoPlayer.targetTexture.Release();
        }

        /// <summary>
        /// Update is called every frame, if the MonoBehaviour is enabled.
        /// </summary>
        void Update()
        {
            if(!isCanClick)
                return;
            if(Input.GetMouseButton(0) || Input.GetKey(KeyCode.Return))
            {
                OnClick();
            }
        }

        public void OnClick() {
            DOTween.Kill("title");
            imgTitle_en.DOFade(1, 0f);
            isCanClick = false;
            
            Invoke("PlayVideo", 1f);
            StartCoroutine("GameSelectLV", videoPlayer.clip.length + 1f);

            imgBlack.gameObject.SetActive(true);
            imgBlack.DOFade(1, 0.5f).SetDelay(0.5f);
            audioSource.DOFade(0, 1f);

            audioUtil.PlayOneShot(AudioUtil.GameStart);
        }

        public void PlayVideo() {
            videoPlayer.Play();
            Invoke("ShowVideo", 0.5f);
        }
        
        public void ShowVideo() {
            imgRaw.gameObject.SetActive(true);
        }

        IEnumerator GameSelectLV(float _time) {
            yield return new WaitForSeconds(_time);
            videoPlayer.targetTexture.Release();
            SceneManager.LoadScene(1);
        }
    }
}

