using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.SceneManagement;
using UnityEngine.Video;

namespace Penguin {
    public class GameLvController : MonoBehaviour
    {
        public LoadingController loadingController;

        public Image[] btnLv;
        public Sprite sprite_NotOpen;
        public Sprite[] sprite_lv;

        public VideoPlayer videoPlayer;
        public RawImage imgRaw;
        public Image imgBlack;

        private GameUtil gameUtil;
        private AudioUtil audioUtil;

        /// <summary>
        /// Awake is called when the script instance is being loaded.
        /// </summary>
        void Awake()
        {
            gameUtil = GameUtil.GetInstance();
            audioUtil = AudioUtil.GetInstance();
        }
        // Start is called before the first frame update
        void Start()
        {
            videoPlayer.targetTexture.Release();
        }

        // Update is called once per frame
        void Update()
        {
            
        }

        /// <summary>
        /// This function is called when the object becomes enabled and active.
        /// </summary>
        void OnEnable()
        {
            if(gameUtil.Lv_Open >= 7)
            {
                gameUtil.Lv_Open = 6;
                PlayVideo();
                return;
            }

            UpdateSprite();
            for(int i = 0; i < btnLv.Length; i++)
            {
                if(btnLv[i].sprite != sprite_NotOpen)
                    btnLv[i].rectTransform.DOAnchorPosY(btnLv[i].rectTransform.anchoredPosition.y - 10f, 1f).SetEase(Ease.InOutQuad).SetLoops(-1, LoopType.Yoyo);
            }
        }

        public void OnLvClick(int _lv)
        {
            if(_lv <= gameUtil.Lv_Open)
            {
                StartCoroutine("LoadScene", _lv);
            }
        }

        IEnumerator LoadScene(int _lv)
        {
            audioUtil.PlayOneShot(AudioUtil.GameSelect);
            loadingController.ShowLoading();
            yield return new WaitForSeconds(0.5f);
            SceneManager.LoadSceneAsync(_lv + 1);
        }

        void UpdateSprite() {
            for(int i = 0; i < btnLv.Length; i++)
            {
                if(i+1 <= gameUtil.Lv_Open)
                {
                    btnLv[i].sprite = sprite_lv[i];
                } else {
                    btnLv[i].sprite = sprite_NotOpen;
                }
            }
        }
        
        public void PlayVideo() {
            GameObject.Find("audioBg").GetComponent<AudioSource>().volume = 0;
            imgBlack.gameObject.SetActive(true);
            videoPlayer.Play();
            Invoke("ShowVideo", 0.5f);
            StartCoroutine("BackToTitle", videoPlayer.clip.length + 1f);
        }

        public void ShowVideo() {
            imgRaw.gameObject.SetActive(true);
        }

        IEnumerator BackToTitle(float _time) {
            yield return new WaitForSeconds(_time);
            SceneManager.LoadScene(0);
        }
    }
}