using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Penguin {
    public class BtnController : MonoBehaviour
    {
        
        [Header("Draw")]
        public Image btnPlay;
        public Image btnDelete;
        public Sprite sprite_btnPlay, sprite_btnPause, sprite_btnDelete;
        private bool isDrawing_Playing = false;
        
        [Header("Play & Draw")]
        public Image btnPlayDraw;
        public Sprite sprite_Play, sprite_Draw;
        private bool isPlayingMode = true;

        
        [Header("RePlay")]
        public Image btnReplay;

        public GameController gameController;
        // Start is called before the first frame update
        void Start()
        {
            
        }

        // Update is called once per frame
        void Update()
        {
            
        }

        public void OnPlayClick() {
            if(!isDrawing_Playing)
            {
                isDrawing_Playing = true;
                btnPlay.sprite = sprite_btnPause;
                gameController.timerGlobalController.StartTimer();

                gameController.ResumeDrawing();
                gameController.ClearTestingBrush();
            } else {
                isDrawing_Playing = false;
                btnPlay.sprite = sprite_btnPlay;
                gameController.timerGlobalController.StopTimer();

                gameController.PauseDrawing();
            }
        }

        public void StartTimer() {
            gameController.timerGlobalController.StartTimer();
        }

        public void OnDeleteClick() {
            isDrawing_Playing = false;
            btnPlay.sprite = sprite_btnPlay;
            //gameController.drawingManager.lineLength = 0;
            gameController.ClearListAndDestroy();
            gameController.timerGlobalController.StopTimer();
            gameController.timerGlobalController.ResetTimer();
            gameController.timerGlobalController.StartTimer();
        }

        public void OnPlayDrawClick() {
            if(!isPlayingMode)
            {
                // change play mode
                gameController.ChangeGameMode(Mode.Playing);
                isPlayingMode = true;

            } else {
                // change draw mode
                gameController.ChangeGameMode(Mode.Drawing);
                isPlayingMode = false;
                isDrawing_Playing = false;
                
            }
        }

        public void OnReplay() {
            gameController.Replay();
        }
    }
}