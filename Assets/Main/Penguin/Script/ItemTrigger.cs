using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace Penguin {
    public class ItemTrigger : MonoBehaviour
    {
        public bool isTrueItem = false;
        // Start is called before the first frame update
        void Start()
        {
            transform.DOLocalMoveY(transform.localPosition.y + 0.3f, 3f).SetEase(Ease.InOutQuad).SetLoops(-1, LoopType.Yoyo);
        }

        // Update is called once per frame
        void Update()
        {
            
        }

        /// <summary>
        /// Sent when another object enters a trigger collider attached to this
        /// object (2D physics only).
        /// </summary>
        /// <param name="other">The other Collider2D involved in this collision.</param>
        void OnTriggerEnter2D(Collider2D other)
        {
            if(other.tag == "Player")
            {
                GameObject.Find("Controller").GetComponent<GameController>().GetItem(isTrueItem);
                gameObject.SetActive(false);
            }
        }
    }
}
