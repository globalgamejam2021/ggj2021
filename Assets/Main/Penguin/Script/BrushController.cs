using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace Penguin {
    public class BrushController : MonoBehaviour
    {
        public CircleTimer circleTimer;
        public float InkLength;

        public DrawingManager drawingManager;
        // Start is called before the first frame update
        void Start()
        {
            
        }

        // Update is called once per frame
        void Update()
        {
            
        }

        public void RestoreInk()
        {
            drawingManager.RestoreInk(InkLength);
        }

        /// <summary>
        /// This function is called when the object becomes enabled and active.
        /// </summary>
        void OnEnable()
        {
            if(circleTimer != null)
                circleTimer.Appear();
        }
    }
}
