using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Penguin {
    public class BtnTrigger : MonoBehaviour
    {
        public SpriteRenderer spriteRenderer;
        public Sprite sprite_triggerOn, sprite_triggerOff;
        public PolygonCollider2D colliderBtnOn;
        public BoxCollider2D colliderBtnOff;

        public bool isBtnClicked = false;

        public List<Collider2D> curObject = new List<Collider2D>();
        // Start is called before the first frame update
        void Start()
        {
            
        }

        // Update is called once per frame
        void Update()
        {
            // isObjectStay = false;
        }

        /// <summary>
        /// LateUpdate is called every frame, if the Behaviour is enabled.
        /// It is called after all Update functions have been called.
        /// </summary>
        void LateUpdate()
        {
            /*
            if(isObjectStay)
            {
                if(!isBtnClicked)
                {
                    Debug.Log("Button clicked");
                    isBtnClicked = true;
                    colliderBtnOn.enabled = false;
                    colliderBtnOff.enabled = true;
                    spriteRenderer.sprite = sprite_triggerOff;
                }
            } else {
                if(isBtnClicked)
                {
                    isBtnClicked = false;
                    colliderBtnOn.enabled = true;
                    colliderBtnOff.enabled = false;
                    spriteRenderer.sprite = sprite_triggerOn;
                }
            }
            */
        }

        /// <summary>
        /// Sent when another object enters a trigger collider attached to this
        /// object (2D physics only).
        /// </summary>
        /// <param name="other">The other Collider2D involved in this collision.</param>
        void OnTriggerEnter2D(Collider2D other)
        {
            // check if already add to list
            for(int i = 0; i < curObject.Count; i++)
            {
                if(curObject[i] == other)
                {
                    return;
                }
            }
            curObject.Add(other);

            if(!isBtnClicked)
            {
                isBtnClicked = true;
                colliderBtnOn.enabled = false;
                colliderBtnOff.enabled = true;
                spriteRenderer.sprite = sprite_triggerOff;
            }
        }

        /// <summary>
        /// Sent when another object leaves a trigger collider attached to
        /// this object (2D physics only).
        /// </summary>
        /// <param name="other">The other Collider2D involved in this collision.</param>
        void OnTriggerExit2D(Collider2D other)
        {
            // check if already exit from list
            for(int i = 0; i < curObject.Count; i++)
            {
                if(curObject[i] == other)
                {
                    curObject.RemoveAt(i);
                    break;
                }
            }

            if(curObject.Count > 0)
                return;
            
            isBtnClicked = false;
            colliderBtnOn.enabled = true;
            colliderBtnOff.enabled = false;
            spriteRenderer.sprite = sprite_triggerOn;
        }

        /*
        /// <summary>
        /// Sent when an incoming collider makes contact with this object's
        /// collider (2D physics only).
        /// </summary>
        /// <param name="other">The Collision2D data associated with this collision.</param>
        void OnCollisionEnter2D(Collision2D other)
        {
            Debug.Log("collision Enter");
            
            isBtnClicked = true;
            colliderBtnOn.enabled = false;
            colliderBtnOff.enabled = true;
            spriteRenderer.sprite = sprite_triggerOff;
        }

        /// <summary>
        /// Sent when a collider on another object stops touching this
        /// object's collider (2D physics only).
        /// </summary>
        /// <param name="other">The Collision2D data associated with this collision.</param>
        void OnCollisionExit2D(Collision2D other)
        {
            Debug.Log("collision Exit");
            isBtnClicked = false;
            colliderBtnOn.enabled = true;
            colliderBtnOff.enabled = false;
            spriteRenderer.sprite = sprite_triggerOn;
        }
        */

        /*
        /// <summary>
        /// Sent each frame where another object is within a trigger collider
        /// attached to this object (2D physics only).
        /// </summary>
        /// <param name="other">The other Collider2D involved in this collision.</param>
        void OnTriggerStay2D(Collider2D other)
        {
            Debug.Log("Stay " + other.name);
            isObjectStay = true;
        }
        */
    }
}
