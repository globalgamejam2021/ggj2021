using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace Penguin {
    public class Fireball : MonoBehaviour
    {
        /// <summary>
        /// Start is called on the frame when a script is enabled just before
        /// any of the Update methods is called the first time.
        /// </summary>
        void Start()
        {
            transform.DOMoveX(transform.position.x + -15f, 2f).OnComplete(()=>{
                Destroy(gameObject);
            });
            transform.DOScale(Vector3.zero,2f).SetEase(Ease.InQuad);
            StartCoroutine("Anim");
        }

        IEnumerator Anim() {
            transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y * -1, transform.localScale.z);
            yield return new WaitForSeconds(0.1f);
            transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y * -1, transform.localScale.z);
            yield return new WaitForSeconds(0.1f);

            StartCoroutine("Anim");
        }

        /// <summary>
        /// Sent each frame where another object is within a trigger collider
        /// attached to this object (2D physics only).
        /// </summary>
        /// <param name="other">The other Collider2D involved in this collision.</param>
        void OnTriggerStay2D(Collider2D other)
        {
            if(other.gameObject.layer == 8)
            {
                Destroy(gameObject);
            }
        }

        /// <summary>
        /// Sent when another object enters a trigger collider attached to this
        /// object (2D physics only).
        /// </summary>
        /// <param name="other">The other Collider2D involved in this collision.</param>
        void OnTriggerEnter2D(Collider2D other)
        {
            if(other.tag == "Player")
            {
                GameController _go = GameObject.Find("Controller").GetComponent<GameController>();
                if(_go.curMode == Mode.Playing)
                    _go.Replay();
            }
        }
    }
}