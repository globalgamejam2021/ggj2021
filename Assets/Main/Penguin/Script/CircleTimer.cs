using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace Penguin {
    public class CircleTimer : MonoBehaviour
    {
        public Image imgCircleTime;
        public float Time = 3f;
        private Tween tween;

        public GameObject Brush;

        public bool WillDestroy = false;
        // Start is called before the first frame update
        void Start()
        {
            Appear();
        }

        // Update is called once per frame
        void Update()
        {
            
        }

        /// <summary>
        /// LateUpdate is called every frame, if the Behaviour is enabled.
        /// It is called after all Update functions have been called.
        /// </summary>
        void LateUpdate()
        {
            transform.eulerAngles = Vector3.zero;
        }

        public void Appear() {
            imgCircleTime.fillAmount = 1;
            Brush.transform.SetParent(null);
            Brush.gameObject.SetActive(true);
            tween = imgCircleTime.DOFillAmount(0, Time).OnComplete(()=>{
                Brush.GetComponent<Rigidbody2D>().Sleep();
                Brush.gameObject.SetActive(false);
                Brush.GetComponent<BrushController>().RestoreInk();
                if(WillDestroy)
                    Destroy(Brush);
                else
                    Brush.transform.SetParent(GameObject.Find("Brush_Used").transform);
            });
        }

        public void Pause() {
            tween.Pause();
        }

        public void Resume() {
            tween.Play();
        }

        public void DestroyNow() {
            tween.Kill();
            Destroy(Brush.gameObject);
        }

        public void DisableNow() {
            if(!Brush.activeInHierarchy)
                return;
            tween.Kill();
            Brush.GetComponent<Rigidbody2D>().Sleep();
            Brush.gameObject.SetActive(false);
            Brush.GetComponent<BrushController>().RestoreInk();
            Brush.transform.SetParent(GameObject.Find("Brush_Used").transform);
        }
    }
    
}
