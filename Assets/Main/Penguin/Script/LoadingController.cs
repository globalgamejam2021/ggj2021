using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace Penguin {
    public class LoadingController : MonoBehaviour
    {
        public GameObject Loading;
        public Image imgLoading;

        /// <summary>
        /// Start is called on the frame when a script is enabled just before
        /// any of the Update methods is called the first time.
        /// </summary>
        void Start()
        {
            imgLoading.rectTransform.DORotate(new Vector3(0, 0, -360f), 1f, RotateMode.FastBeyond360).SetLoops(-1);
        }

        public void ShowLoading() {
            Loading.gameObject.SetActive(true);
        }

        public void DisableLoading() {
            Loading.gameObject.SetActive(false);
        }
    }
}
