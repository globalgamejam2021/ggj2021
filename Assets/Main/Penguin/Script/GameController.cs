using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.SceneManagement;

namespace Penguin {
    public enum Mode {
        Drawing,
        Playing
    }
    public class GameController : MonoBehaviour
    {
        public SpriteRenderer donut;
        public Text txtWin;

        public Mode curMode = Mode.Drawing;

        public bool isGetItem = false;
        public bool isGetTrueItem = false;

        public bool isEnd = false;

        public bool isReplaying = false;

        public BtnController btnController;
        public Image imgBorder;

        public Transform Brush_Used;

        public LoadingController loadingController;

        // Character
        public twodotzero.riceController Character;
        public Transform StartPoint;
        // print
        public int nextBrush = 0;

        public Texture2D cursorTexture;


        [Header("Drawing")]
        public DrawingManager drawingManager;
        public Image imgInk;
        public Image imgInkBorder;

        [Header("Timer")]
        public TimerGlobalController timerGlobalController;
        [Header("Door")]
        public ExitDoor exitDoor;

        [Header("Video")]
        public VideoController videoController;

        private GameUtil gameUtil;
        private AudioUtil audioUtil;

        /// <summary>
        /// Awake is called when the script instance is being loaded.
        /// </summary>
        void Awake()
        {
            gameUtil = GameUtil.GetInstance();
            audioUtil = AudioUtil.GetInstance();
            Screen.SetResolution(1920, 1080, true);
        }
        // Start is called before the first frame update
        void Start()
        {
            ChangeGameMode(Mode.Playing);
        }

        // Update is called once per frame
        void Update()
        {
            InkUpdate();
            PrintBrush();

            
            if(!Input.GetMouseButton(0))
            {
                if(Input.GetKeyUp(KeyCode.R))
                {
                    if(curMode == Mode.Drawing)
                    {
                        ChangeGameMode(Mode.Playing);
                    } else {
                        ChangeGameMode(Mode.Drawing);
                    }
                }
            }
            
            if(Input.GetKeyUp(KeyCode.Escape))
            {
                SceneManager.LoadScene(1);
            }
        }

        public void ChangeGameMode(Mode _mode) {
            curMode = _mode;
            switch(curMode)
            {
                default:
                    curMode = Mode.Drawing;
                    drawingManager.SetIsCanDraw();
                    nextBrush = 0;

                    btnController.btnPlayDraw.sprite = btnController.sprite_Play;

                    btnController.btnPlay.sprite = btnController.sprite_btnPlay;
                    btnController.btnPlay.gameObject.SetActive(false);
                    btnController.btnDelete.gameObject.SetActive(true);
                    
                    btnController.btnReplay.gameObject.SetActive(false);

                    timerGlobalController.StopTimer();
                    timerGlobalController.ResetTimer();
                    timerGlobalController.StartTimer();
                    ClearList();
                    // clear used brush
                    for(int i = 0; i < Brush_Used.childCount; i++)
                    {
                        Destroy(Brush_Used.GetChild(i).gameObject);
                    }

                    imgBorder.gameObject.SetActive(true);

                    Character.UseSpell();

                    Cursor.SetCursor(cursorTexture, new Vector2(21f, 16f), CursorMode.Auto);

                    // reset character position
                    //CharacterReset();

                    //drawingManager.lineLength = 0;

                    //Character.GetComponent<CapsuleCollider2D>().isTrigger = true;
                    //Character.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Kinematic;
                break;
                case Mode.Playing:
                    curMode = Mode.Playing;
                    drawingManager.isCanDraw = false;
                    //Camera.main.transform.DOMove(new Vector3(0, 1, -10), 0.2f);

                    btnController.btnPlayDraw.sprite = btnController.sprite_Draw;

                    btnController.btnPlay.gameObject.SetActive(false);
                    btnController.btnDelete.gameObject.SetActive(false);

                    btnController.btnReplay.gameObject.SetActive(true);

                    //ClearTestingBrush();
                    DisableBrush();
                    timerGlobalController.StopTimer();
                    timerGlobalController.ResetTimer();
                    if(drawingManager.DrawingBrushList.Count > 0)
                    {
                        // play the play mode
                        timerGlobalController.StartTimer();
                    }

                    imgBorder.gameObject.SetActive(false);

                    Character.alive = true;
                    Character.resetToIdle();

                    loadingController.DisableLoading();

                    Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);

                    //drawingManager.lineLength = 0;

                    //Character.GetComponent<CapsuleCollider2D>().isTrigger = false;
                    //Character.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;

                break;
            }
        }

        public void GetItem(bool _isTrueItem) {
            audioUtil.PlayOneShot(AudioUtil.ItemGet);
            isGetItem = true;
            if(isGetTrueItem)
                return;
            isGetTrueItem = _isTrueItem;
        }

        public void Win() {
            if(isEnd)
                return;
            isEnd = true;
            //txtWin.gameObject.SetActive(true);
            timerGlobalController.StopTimer();

            Character.alive = false;
            videoController.ShowAndPlayVideo(isGetTrueItem);
            loadingController.ShowLoading();

            audioUtil.PlayOneShot(AudioUtil.GameEnd);

            if(isGetTrueItem)
            {
                if(gameUtil.Lv_Open <= SceneManager.GetActiveScene().buildIndex)
                {
                    gameUtil.Lv_Open = SceneManager.GetActiveScene().buildIndex;
                }
            }
        }

        // Dead
        public void Replay() {
            if(isReplaying)
                return;
            isReplaying = true;
            timerGlobalController.StopTimer();
            Character.PlayHurt();
            StartCoroutine("ReplayDelay");
        }

        IEnumerator ReplayDelay() {
            yield return new WaitForSeconds(1f);
            ChangeGameMode(Mode.Playing);

            // reset character position
            CharacterReset();

            DisableBrush();
            ClearListAndDestroy();

            nextBrush = 0;

            timerGlobalController.StopTimer();
            timerGlobalController.ResetTimer();
            // play the play mode
            //timerGlobalController.StartTimer();

            Camera.main.GetComponent<twodotzero.tracking>().ResetToTarget();

            
            GameObject.Find("audioBg").GetComponent<AudioSource>().volume = 1;

            isEnd = false;
            yield return new WaitForSeconds(0.01f);
            isReplaying = false;
        }

        public void InkUpdate() {
            imgInk.fillAmount = (drawingManager.lenghLimit - drawingManager.lineLength) / drawingManager.lenghLimit;
        }

        public void CharacterReset() {
            Character.transform.position = StartPoint.position;
            Character.direction = 1;
            Character.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
            Character.GetComponent<Rigidbody2D>().angularVelocity = 0;
        }

        public void ClearList() {
            drawingManager.ClearList();
        }

        public void ClearListAndDestroy() {
            drawingManager.ClearListAndDestroy();

            timerGlobalController.StopTimer();
        }

        public void PrintBrush() {
            if(curMode == Mode.Drawing)
                return;
            if(nextBrush >= drawingManager.DrawingBrushList.Count)
                return;
            if(timerGlobalController.Time >= drawingManager.DrawingBrushList[nextBrush].DrawingTime)
            {
                drawingManager.DrawingBrushList[nextBrush].GO_Brush.transform.position = drawingManager.DrawingBrushList[nextBrush].OriPos;
                drawingManager.DrawingBrushList[nextBrush].GO_Brush.transform.eulerAngles = drawingManager.DrawingBrushList[nextBrush].OriRotate;
                Rigidbody2D _r2d = drawingManager.DrawingBrushList[nextBrush].GO_Brush.GetComponent<Rigidbody2D>();
                _r2d.velocity = drawingManager.DrawingBrushList[nextBrush].lastVelocity;
                _r2d.bodyType = RigidbodyType2D.Dynamic;
                drawingManager.DrawingBrushList[nextBrush].GO_Brush.GetComponent<Rigidbody2D>().WakeUp();
                drawingManager.DrawingBrushList[nextBrush].GO_Brush.gameObject.SetActive(true);
                drawingManager.lineLength += drawingManager.DrawingBrushList[nextBrush].Length;
                LineRenderer _lr = drawingManager.DrawingBrushList[nextBrush].GO_Brush.GetComponent<LineRenderer>();
                Gradient gradient = _lr.colorGradient;
                gradient.SetKeys(
                    new GradientColorKey[] { new GradientColorKey(new Color(4f / 255f, 22f / 255f, 164f / 255f), 0f), new GradientColorKey(new Color(4f / 255f, 22f / 255f, 164f / 255f), 1f)},
                    new GradientAlphaKey[] { new GradientAlphaKey(1f, 0.0f), new GradientAlphaKey(1f, 1.0f) }
                );
                _lr.colorGradient = gradient;
                if(nextBrush == 0)
                    audioUtil.PlayOneShot(AudioUtil.Char_Wa);
                nextBrush++;
                if(nextBrush >= drawingManager.DrawingBrushList.Count)
                    timerGlobalController.StopTimer();
            }
        }
        public void ClearTestingBrush() {
            drawingManager.ClearTestingBrushOnly();
        }

        public void DisableBrush() {
            drawingManager.DisableBrush();
        }

        public void PauseDrawing() {
            for(int i = 0; i < drawingManager.DrawingBrushList.Count; i++)
            {
                Rigidbody2D _r2d = drawingManager.DrawingBrushList[i].GO_Brush.GetComponent<Rigidbody2D>();
                drawingManager.DrawingBrushList[i].lastVelocity = _r2d.velocity;
                //drawingManager.DrawingBrushList[i].lastAngularVelocity = _r2d.angularVelocity;
                
                _r2d.velocity = Vector3.zero;
                _r2d.angularVelocity = 0;

                _r2d.bodyType = RigidbodyType2D.Kinematic;
                drawingManager.DrawingBrushList[i].brushController.circleTimer.Pause();
            }
        }

        public void ResumeDrawing() {
            for(int i = 0; i < drawingManager.DrawingBrushList.Count; i++)
            {
                Rigidbody2D _r2d = drawingManager.DrawingBrushList[i].GO_Brush.GetComponent<Rigidbody2D>();
                _r2d.velocity = drawingManager.DrawingBrushList[i].lastVelocity;
                _r2d.bodyType = RigidbodyType2D.Dynamic;
                drawingManager.DrawingBrushList[i].brushController.circleTimer.Resume();
            }
        }

        // 無墨水 shake ui
        public void ShakeNoInk() {
            DOTween.Kill("ShakeNoInk");
            imgInkBorder.rectTransform.anchoredPosition = new Vector2(805.6f, -94.7f);
            imgInkBorder.rectTransform.DOShakeAnchorPos(0.3f, new Vector2(15, 5), 30, 90, false, false).SetId("ShakeNoInk");
        }
    }
}
