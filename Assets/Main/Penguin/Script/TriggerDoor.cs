using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace Penguin {
    
    public class TriggerDoor : MonoBehaviour
    {
        public float MaxLength = 3.33f;
        private bool isDoorOpened = false;
        public BtnTrigger[] effect_triggers;

        private Tween tween;
        // Start is called before the first frame update
        void Start()
        {
            
        }

        // Update is called once per frame
        void Update()
        {
            CheckOpenCloseDoor();
        }

        void CheckOpenCloseDoor() {
            if(!isDoorOpened)
            {
                bool _isAllTriggerPress = true;
                for(int i = 0; i < effect_triggers.Length; i++)
                {
                    if(!effect_triggers[i].isBtnClicked)
                        _isAllTriggerPress = false;
                }
                if(_isAllTriggerPress)
                {
                    OpenDoor();
                }
            } else {
                bool _isAllTriggerPress = true;
                for(int i = 0; i < effect_triggers.Length; i++)
                {
                    if(!effect_triggers[i].isBtnClicked)
                        _isAllTriggerPress = false;
                }
                if(!_isAllTriggerPress)
                {
                    CloseDoor();
                }
            }
        }

        void OpenDoor() {
            if(isDoorOpened)
                return;
            tween.Kill();
            tween = transform.DOScaleY(0, 0.1f * (1 - ((MaxLength - transform.localScale.y) / 3.33f)));
            isDoorOpened = true;
        }

        void CloseDoor() {
            if(!isDoorOpened)
                return;
            tween.Kill();
            tween = transform.DOScaleY(MaxLength, 0.1f * ((MaxLength - transform.localScale.y) / 3.33f));
            isDoorOpened = false;
        }
    }
}
