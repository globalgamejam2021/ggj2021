using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpellBorder : MonoBehaviour
{
    private Image image;
    public Sprite[] sprite_borders;

    private int curFrame = 0;
    // Start is called before the first frame update
    void Start()
    {
        image = GetComponent<Image>();
        Invoke("Anim", 0.2f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void Anim() {
        image.sprite = sprite_borders[curFrame];
        curFrame++;
        if(curFrame >= sprite_borders.Length)
            curFrame = 0;
        Invoke("Anim", 0.2f);
    }
}
