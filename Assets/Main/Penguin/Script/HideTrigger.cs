using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Penguin {
    
    public class HideTrigger : MonoBehaviour
    {
        public GameObject hideObject;
        // Start is called before the first frame update
        void Start()
        {
            
        }

        // Update is called once per frame
        void Update()
        {
            
        }

        void OnTriggerEnter2D(Collider2D other)
        {
            if(other.tag == "Player")
                hideObject.gameObject.SetActive(true);
        }
    }
}
