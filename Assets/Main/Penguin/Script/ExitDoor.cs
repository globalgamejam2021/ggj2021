using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace Penguin {
    public class ExitDoor : MonoBehaviour
    {
        public SpriteRenderer door_tp;

        public GameController gameController;

        private bool isDoorAlready = false;
        // Start is called before the first frame update
        void Start()
        {
            door_tp.transform.DORotate(new Vector3(0, 0, -360f), 1.5f, RotateMode.FastBeyond360).SetLoops(-1, LoopType.Restart);
            door_tp.gameObject.SetActive(false);
            door_tp.transform.localScale = Vector3.zero;
        }

        // Update is called once per frame
        void Update()
        {
            if(gameController.isGetItem)
            {
                DoorOpen();
            }
        }

        void DoorOpen() {
            if(isDoorAlready)
                return;
            isDoorAlready = true;
            door_tp.gameObject.SetActive(true);
            door_tp.transform.DOScale(Vector3.one, 1f).SetEase(Ease.OutBack);
            GetComponent<BoxCollider2D>().enabled = true;
        }

        /// <summary>
        /// Sent when another object enters a trigger collider attached to this
        /// object (2D physics only).
        /// </summary>
        /// <param name="other">The other Collider2D involved in this collision.</param>
        void OnTriggerEnter2D(Collider2D other)
        {
            if(other.tag == "Player")
            {
                if(!isDoorAlready)
                    return;
                gameController.Win();
            }
        }
    }
}
