using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace Penguin {
    public class MonsterAttack : MonoBehaviour
    {
        private SpriteRenderer spriteRenderer;
        public Sprite monster_idle, monster_attack;
        public GameObject FireBallPrefab;
        // Start is called before the first frame update
        void Start()
        {
            transform.DOMoveY(transform.position.y + 1.5f, 3f).SetEase(Ease.InOutQuad).SetLoops(-1, LoopType.Yoyo);
            spriteRenderer = GetComponent<SpriteRenderer>();
            spriteRenderer.sprite = monster_idle;
            AttackNow();
        }

        // Update is called once per frame
        void Update()
        {
            
        }

        void AttackNow()
        {
            StartCoroutine("Attack");
        }

        IEnumerator Attack() {
            spriteRenderer.sprite = monster_attack;
            // shot ball
            GameObject _fireBall = Instantiate(FireBallPrefab) as GameObject;
            _fireBall.transform.position = new Vector3(transform.position.x - 1.25f, transform.position.y, transform.position.z);
            _fireBall.transform.eulerAngles = FireBallPrefab.transform.eulerAngles;
            _fireBall.transform.localScale = FireBallPrefab.transform.localScale;
            yield return new WaitForSeconds(0.5f);
            spriteRenderer.sprite = monster_idle;

            yield return new WaitForSeconds(2f);
            StartCoroutine("Attack");
        }

        void OnTriggerEnter2D(Collider2D other)
        {
            if(other.tag == "Player")
            {
                GameController _go = GameObject.Find("Controller").GetComponent<GameController>();
                if(_go.curMode == Mode.Playing)
                    _go.Replay();
            }
        }
    }

}