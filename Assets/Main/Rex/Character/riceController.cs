﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace twodotzero
{
    public class riceController : MonoBehaviour
    {
        public float movePower = 10f;
        public float jumpPower = 15f; //Set Gravity Scale in Rigidbody2D Component to 5
        public SpriteRenderer[] skinColors;
        private Rigidbody2D rb;
        private Animator anim;
        Vector3 movement;
        public int direction = 1;
        bool isJumping = false;
        public bool alive = true;

        private Vector3 oriScale;
        public Penguin.GameController gameController;

        public Penguin.DrawingManager drawingManager;

        public bool isAttack = false;

        private AudioUtil audioUtil;
        // Start is called before the first frame update
        void Start()
        {
            audioUtil = AudioUtil.GetInstance();
            rb = GetComponent<Rigidbody2D>();
            anim = GetComponent<Animator>();
            oriScale = transform.localScale;
        }

        private void Update()
        {
            
            Restart();
            if (alive)
            {
                Spelling();
                //Hurt();
                //Die();
                //Attack();
                Jump();
                Run();
                //Win();
            }
        }
        private void OnTriggerEnter2D(Collider2D other)
        {
            if(other.tag != "DeathTrigger" && other.tag != "NotForJump")
            {
                if(other.tag == "Ground" || other.gameObject.layer == 8){
                anim.SetBool("isJump", false);
                anim.SetBool("isGround", true);
                }
            }
        }
        private void OnTriggerExit2D(Collider2D other) {
            if(other.gameObject.layer == 8 || other.tag == "Ground" ) {
                anim.SetBool("isGround",false);
                anim.SetBool("isJump",true);
            }
        }

        void Win() {
            if (Input.GetKeyDown(KeyCode.Alpha4))
            {
                anim.SetTrigger("win");
            }
        }
        void Run()
        {
            Vector3 moveVelocity = Vector3.zero;
            anim.SetBool("isWalk", false);

              if(gameController.curMode == Penguin.Mode.Playing)
              {
                if (Input.GetAxisRaw("Horizontal") < 0)
                {
                    direction = -1;
                    moveVelocity = Vector3.left;
                    
                    transform.localScale = new Vector3(oriScale.x * direction, oriScale.y, oriScale.z);
                    
                    if (!anim.GetBool("isJump"))
                        anim.SetBool("isWalk", true);
                    

                }
                if (Input.GetAxisRaw("Horizontal") > 0)
                {
                    direction = 1;
                    moveVelocity = Vector3.right;

                    transform.localScale = oriScale;
                    if (!anim.GetBool("isJump"))
                        anim.SetBool("isWalk", true);

                }
                transform.position += moveVelocity * movePower * Time.deltaTime;
             }


            transform.localScale = new Vector3(oriScale.x * direction, oriScale.y, oriScale.z);
        }
        void Jump()
        {
            if(gameController.curMode != Penguin.Mode.Playing)
                return;
            if ((Input.GetButtonDown("Jump") || Input.GetAxisRaw("Vertical") > 0)
            && !anim.GetBool("isJump"))
            {
                isJumping = true;
                anim.SetBool("isJump", true);
                anim.SetBool("isGround",false);

                audioUtil.PlayOneShot(AudioUtil.Char_Jump);
            }
            
            if (!isJumping)
            {
                return;
            }
            
            rb.velocity = Vector2.zero;

            Vector2 jumpVelocity = new Vector2(0, jumpPower);
            rb.AddForce(jumpVelocity, ForceMode2D.Impulse);

            isJumping = false;
        }
        void Attack()
        {
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                anim.SetTrigger("attack");
            }
        }

        public void UseSpell() {
            anim.SetTrigger("attack");
            audioUtil.PlaySpell();
        }

        public void Spelling() {
            if(gameController == null || drawingManager == null)
                return;
            if(gameController.curMode == Penguin.Mode.Drawing)
            {
                if(drawingManager.mousePointer.transform.position.x > transform.position.x)
                {
                    direction = 1;
                } else {
                    direction = -1;
                }
            }
        }
    
        IEnumerator attackToFalse () {
            yield return new WaitForSeconds(0.2f);
            anim.SetBool("isAttack", false);
            alive = true;
        }
        void Hurt()
        {
            if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                PlayHurt();
            }
        }

        public void PlayHurt() {
            alive = false;
            anim.SetTrigger("hurt");
            if (direction == 1)
                rb.AddForce(new Vector2(-5f, 1f), ForceMode2D.Impulse);
            else
                rb.AddForce(new Vector2(5f, 1f), ForceMode2D.Impulse);
            audioUtil.PlayDie();
        }
        void disableSkin() {
            foreach(SpriteRenderer color in skinColors){
                color.enabled = false;
            }
        }
        void enableSkin() {
            foreach(SpriteRenderer color in skinColors){
                color.enabled = true;
            }
        }
        void Die()
        {
            if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                anim.SetTrigger("die");
                alive = false;
            }
        }
        void Restart()
        {
            if (Input.GetKeyDown(KeyCode.Alpha0))
            {
                enableSkin();
                anim.SetTrigger("idle");
                alive = true;
            }
        }
        public void resetToIdle () {
            enableSkin();    
            anim.SetTrigger("idle");
            alive = true;
        }
    }
}