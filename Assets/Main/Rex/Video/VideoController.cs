using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.SceneManagement;

namespace Penguin {
    public class VideoController : MonoBehaviour
    {
        public GameController gameController; 
        public VideoPlayer videoPlayer;
        public GameObject Go_Video;

        public VideoClip winClip;
        public VideoClip loseClip;
        // Start is called before the first frame update
        void Start()
        {
            videoPlayer.targetTexture.Release();
        }

        // Update is called once per frame
        void Update()
        {
            
        }

        public void ShowAndPlayVideo(bool _isWin) {
            Debug.Log("ShowAndPlayVideo");
            if(_isWin)
            {
                videoPlayer.clip = winClip;
                StartCoroutine("BackToGameLVSelection", videoPlayer.clip.length + 1f);
            } else {
                videoPlayer.clip = loseClip;
                StartCoroutine("RestartGame", videoPlayer.clip.length + 1f);
            }
            videoPlayer.Play();
            Invoke("ShowVideo", 0.5f);
        }

        public void ShowVideo() {
            Go_Video.gameObject.SetActive(true);
            GameObject.Find("audioBg").GetComponent<AudioSource>().volume = 0;
        }

        IEnumerator RestartGame(float _time) {
            yield return new WaitForSeconds(_time);
            Go_Video.gameObject.SetActive(false);
            gameController.Replay();

            videoPlayer.targetTexture.Release();
        }

        
        IEnumerator BackToGameLVSelection(float _time) {
            yield return new WaitForSeconds(_time);
            

            videoPlayer.targetTexture.Release();
            SceneManager.LoadScene(1);
        }
    }
}
