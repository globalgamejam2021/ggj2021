using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace twodotzero {
public class tracking : MonoBehaviour
    {
        
        public Penguin.GameController gameController;

        [SerializeField]
        float xOffset;
        [SerializeField]
        float yOffset;
        // Update is called once per frame
        [SerializeField]
        protected Transform trackingTarget;
        [SerializeField]
        protected float followSpeed;
        // ...
        [SerializeField]
        protected bool isXLocked = false;

        [SerializeField]
        protected bool isYLocked = false;
        [SerializeField]
        public float leftLimit;
        [SerializeField]
        public float rightLimit;
        [SerializeField]
        public float topLimit;
        [SerializeField]
        public float bottomLimit;
        // ...


        void LateUpdate()
        {
            if(gameController.curMode == Penguin.Mode.Playing)
            {
                float xTarget = trackingTarget.position.x + xOffset;
                float yTarget = trackingTarget.position.y + yOffset;
                float xNew = transform.position.x;
                if (!isXLocked)
                {
                    xNew = Mathf.Lerp(transform.position.x, xTarget, Time.deltaTime * followSpeed);
                }

                float yNew = transform.position.y;
                if (!isYLocked)
                {
                    yNew = Mathf.Lerp(transform.position.y, yTarget, Time.deltaTime * followSpeed);
                }
                //float xNew = Mathf.Lerp(transform.position.x, xTarget, Time.deltaTime * followSpeed);
                //float yNew = Mathf.Lerp(transform.position.y, yTarget, Time.deltaTime * followSpeed);

                
                transform.position = new Vector3 (
                    Mathf.Clamp(xNew, leftLimit, rightLimit),
                    Mathf.Clamp(yNew, bottomLimit, topLimit),
                    transform.position.z
                );
            }
        }

        public void ResetToTarget() {
            float xTarget = trackingTarget.position.x + xOffset;
            float yTarget = trackingTarget.position.y + yOffset;
            transform.position = new Vector3(xTarget, yTarget, transform.position.z);
        }
    }
}
